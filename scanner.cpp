// Quinn Foley
// Homework 6
// TCP Port Scanner
#include <iostream> 
#include <string>
#include <crafter.h>
#include <crafter/Utils/TCPConnection.h>
using namespace std;
using namespace Crafter;

int main(int argc, char *argv[]){
	
    /*Init the Library*/
    InitCrafter();
	
    string iface;
    string t_ip;
    string my_ip;
    short_word s_port;
    short_word e_port;
    if (argc == 5){
        iface = argv[1];
        t_ip = argv[2];
	s_port = atoi(argv[3]);
	e_port = atoi(argv[4]);
	my_ip = GetMyIP(iface);
        
        	
	cout<<"network interface: "<<iface<<endl;
	cout<<"my ip: "<<my_ip<<endl;
	cout<<"target ip: "<<t_ip<<endl;
	cout<<"start port: "<<s_port<<endl;
	cout<<"end port: "<<e_port<<endl;
        sleep(3);    	
    }
    else{
	printf("Usage: ./scanner [network interface] [target ip] [start port] [end port]\n");
	return 0;
    }
    for(int port_iter = s_port; port_iter < e_port; port_iter++){

        /*
        Ethernet ether_header;

        ether_header.SetDestinationMAC(GetMAC(t_ip,iface));  GetMAC will do an ARP request and get that IP address
        ether_header.SetSourceMAC(GetMyMAC());
        */
        IP ip_header;

        ip_header.SetSourceIP(my_ip);
        ip_header.SetDestinationIP(t_ip);

        IPOption security;
        security.SetOption(8);
        security.SetPayload("\x1\x1");

        TCP tcp_header;

        tcp_header.SetSrcPort(RNG16());
        tcp_header.SetDstPort(port_iter);
        tcp_header.SetSeqNumber(RNG32());
        tcp_header.SetFlags(TCP::FIN);
        tcp_header.SetFlags(TCP::PSH);
        tcp_header.SetFlags(TCP::URG);
    
        /* Max segment size option */
        TCPOptionMaxSegSize maxseg;
        maxseg.SetMaxSegSize(1460);

        /* Set some generic option (and if the option holds more data, should be added on the payload) */
        TCPOption wind;
        wind.SetKind(3);
        wind.SetPayload("\x7");

        /* Time stamp option */
        TCPOptionTimestamp tstamp;
        tstamp.SetValue(398303815);

        /* Create a payload */
        RawLayer raw_header;
        raw_header.SetPayload("SomeTCPPayload\n");

        /* Create a packet... */
        Packet packet =  ip_header / security / security / security / tcp_header /
        /* START Option (padding should be controlled by the user) */
        maxseg /            // 4  bytes
        wind /              // 3  bytes
        tstamp /            // 10 bytes                                     
        TCPOption::NOP /    // 3  bytes <-- Padded to a multiple of 32 bits
        TCPOption::NOP /
        TCPOption::EOL /
        /* END Option  TOTAL = 20 bytes */
        raw_header;
    
        /* Send the packet, and wait for an answer.... */
        Packet* pck_rcv = packet.SendRecv(iface,0.01,1);
    
        //packet.Print();

        if(pck_rcv) {

            /* Print all the received packet */
            //pck_rcv->Print();

            TCP* rcv_tcp = pck_rcv->GetLayer<TCP>();
            /* We want to check some option of the SYN/ACK received */
            if(rcv_tcp->RST){ 
                cout << "[@] Port "<<port_iter<<" closed" << endl;
            }
                
        
        }else{
            cout<<"[*] Port "<<port_iter<<" open."<<endl;
        }
    }
    CleanCrafter();
    return 0;
}
